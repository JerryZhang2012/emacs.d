(require 'package)

(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
		("melpa" . "https://melpa.org/packages/")
		("melpa-stable" . "http://stable.melpa.org/packages/")
		("nongnu" . "https://elpa.nongnu.org/nongnu/")
		))

;; use http proxy
(setq url-proxy-services
	  '(("http" . "172.26.16.1:7890")
		("https" . "172.26.16.1:7890")))

;; china
;; (setq package-archives
;;       '(
;; 		("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
;;         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
;; 		("melpa-stable" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/stable-melpa/")
;; 		("nongnu" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu/")
;; 		)
;; 	  )

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-and-compile
  (setq use-package-verbose t)
  (setq use-package-always-pin t)
  (setq use-package-compute-statistics t)
  (setq use-package-hook-name-suffix nil)
  )

(eval-when-compile
  (require 'use-package))

(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'init-base)
(require 'init-keybinding) ;; must be top than other <f1>-><12> binding
(require 'init-dict)
(require 'init-dired)
(require 'init-ui)
(require 'init-edit)
(require 'init-env)
(require 'init-flycheck)
(require 'init-feed)
(require 'init-ibuffer)
(require 'init-vc)
(require 'init-lang)
(require 'init-markdown)
(require 'init-nginx)
(require 'init-db)
(require 'init-docker)
(require 'init-web)
(require 'init-yaml)
(require 'init-plantuml)
(require 'init-org)

;; variables configured via the interactive 'customize' interface
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))
